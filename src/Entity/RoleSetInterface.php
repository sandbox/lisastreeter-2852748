<?php

namespace Drupal\role_sets\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Role set entities.
 */
interface RoleSetInterface extends ConfigEntityInterface {

  /**
   * Returns the set type of the role set.
   *
   * @return string
   *   The set type of the role set.
   */
  public function getSetType();

  /**
   * Sets the set type of the role set.
   *
   * @param string $set_type
   *   The set type of the role set.
   *
   * @return $this
   */
  public function setSetType($set_type);

  /**
   * Returns a list of modules assigned to the role set.
   *
   * @return array
   *   The modules assigned to the role set.
   */
  public function getModules();

  /**
   * Checks if the role set has a module.
   *
   * @param string $module
   *   The module to check for.
   *
   * @return bool
   *   TRUE if the role set has the module, FALSE if not.
   */
  public function hasModule($module);

  /**
   * Add a module to the role set.
   *
   * @param string $module
   *   The module to add.
   *
   * @return $this
   */
  public function addModule($module);

  /**
   * Removes a module from the role set.
   *
   * @param string $module
   *   The module to remove.
   *
   * @return $this
   */
  public function removeModule($module);

  /**
   * Returns a list of packages assigned to the role set.
   *
   * @return array
   *   The packages assigned to the role set.
   */
  public function getPackages();

  /**
   * Checks if the role set has a package.
   *
   * @param string $package
   *   The package to check for.
   *
   * @return bool
   *   TRUE if the role set has the package, FALSE if not.
   */
  public function hasPackage($package);

  /**
   * Add a package to the role set.
   *
   * @param string $package
   *   The package to add.
   *
   * @return $this
   */
  public function addPackage($package);

  /**
   * Removes a package from the role set.
   *
   * @param string $package
   *   The package to remove.
   *
   * @return $this
   */
  public function removePackage($package);

  /**
   * Returns a list of roles assigned to the role set.
   *
   * @return array
   *   The roles assigned to the role set.
   */
  public function getRoles();

  /**
   * Checks if the role set has a role.
   *
   * @param string $role
   *   The role to check for.
   *
   * @return bool
   *   TRUE if the role set has the role, FALSE if not.
   */
  public function hasRole($role);

  /**
   * Add a role to the role set.
   *
   * @param string $role
   *   The role to add.
   *
   * @return $this
   */
  public function addRole($role);

  /**
   * Removes a role from the role set.
   *
   * @param string $role
   *   The role to remove.
   *
   * @return $this
   */
  public function removeRole($role);

}
