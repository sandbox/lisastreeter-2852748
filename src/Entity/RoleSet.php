<?php

namespace Drupal\role_sets\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Role set entity.
 *
 * @ConfigEntityType(
 *   id = "role_set",
 *   label = @Translation("Role set"),
 *   handlers = {
 *     "access" = "Drupal\role_sets\RoleSetAccessControlHandler",
 *     "list_builder" = "Drupal\role_sets\RoleSetListBuilder",
 *     "form" = {
 *       "default" = "Drupal\role_sets\Form\RoleSetForm",
 *       "delete" = "Drupal\role_sets\Form\RoleSetDeleteForm"
 *     },
 *   },
 *   config_prefix = "role_set",
 *   admin_permission = "administer role sets",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "edit-form" = "/admin/people/role_sets/manage/{role_set}",
 *     "delete-form" = "/admin/people/role_sets/manage/{role_set}/delete",
 *     "edit-permissions-form" = "/admin/people/role_sets/permissions/{role_set}",
 *     "collection" = "/admin/people/role_sets"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "set_type",
 *     "roles",
 *     "packages",
 *     "modules",
 *   }
 * )
 */
class RoleSet extends ConfigEntityBase implements RoleSetInterface {

  /**
   * The Role set ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Role set label.
   *
   * @var string
   */
  protected $label;

  /**
   * The Role set type.
   *
   * @var string
   */
  protected $set_type;

  /**
   * The modules belonging to this role set.
   *
   * @var array
   */
  protected $modules = array();

  /**
   * The packages belonging to this role set.
   *
   * @var array
   */
  protected $packages = array();

  /**
   * The roles belonging to this role set.
   *
   * @var array
   */
  protected $roles = array();

  /**
   * {@inheritdoc}
   */
  public function getSetType() {
    return $this->get('set_type');
  }

  /**
   * {@inheritdoc}
   */
  public function setSetType($set_type) {
    $this->set('set_type', $set_type);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getModules() {
    return array_filter($this->modules);
  }

  /**
   * {@inheritdoc}
   */
  public function hasModule($module) {
    return in_array($module, $this->modules);
  }

  /**
   * {@inheritdoc}
   */
  public function addModule($module) {
    if (!$this->hasModule($module)) {
      $this->modules[] = $module;
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function removeModule($module) {
    $this->modules = array_diff($this->modules, array($module));
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPackages() {
    return array_filter($this->packages);
  }

  /**
   * {@inheritdoc}
   */
  public function hasPackage($package) {
    return in_array($package, $this->packages);
  }

  /**
   * {@inheritdoc}
   */
  public function addPackage($package) {
    if (!$this->hasPackage($package)) {
      $this->packages[] = $package;
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function removePackage($package) {
    $this->packages = array_diff($this->packages, array($package));
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getRoles() {
    return array_filter($this->roles);
  }

  /**
   * {@inheritdoc}
   */
  public function hasRole($role) {
    return in_array($role, $this->roles);
  }

  /**
   * {@inheritdoc}
   */
  public function addRole($role) {
    if (!$this->hasRole($role)) {
      $this->roles[] = $role;
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function removeRole($role) {
    $this->roles = array_diff($this->roles, array($role));
    return $this;
  }

}
