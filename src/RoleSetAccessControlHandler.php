<?php

namespace Drupal\role_sets;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the role set entity type.
 *
 * @see \Drupal\persona\Entity\RoleSet
 */
class RoleSetAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    if ($operation == 'view' && $account->hasPermission('view role sets')) {
      $access = AccessResult::allowed();
    }
    else {
      $access = parent::checkAccess($entity, $operation, $account);
    }
    return $access;
  }

}
