<?php

namespace Drupal\role_sets\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\user\PermissionHandlerInterface;
use Drupal\user\RoleStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class RoleSetForm.
 *
 * @package Drupal\role_sets\Form
 */
class RoleSetForm extends EntityForm {

  /**
   * The permission handler.
   *
   * @var \Drupal\user\PermissionHandlerInterface
   */
  protected $permissionHandler;

  /**
   * The role storage.
   *
   * @var \Drupal\user\RoleStorageInterface
   */
  protected $roleStorage;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a new RoleSetForm.
   *
   * @param \Drupal\user\PermissionHandlerInterface $permission_handler
   *   The permission handler.
   * @param \Drupal\user\RoleStorageInterface $role_storage
   *   The role storage.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(PermissionHandlerInterface $permission_handler, RoleStorageInterface $role_storage, ModuleHandlerInterface $module_handler) {
    $this->permissionHandler = $permission_handler;
    $this->roleStorage = $role_storage;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('user.permissions'),
      $container->get('entity.manager')->getStorage('user_role'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $role_set = $this->entity;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#size' => 30,
      '#maxlength' => 64,
      '#default_value' => $role_set->label(),
      '#required' => TRUE,
      '#description' => $this->t('The name for this role set. Example: "Commerce Users", "Commerce Config Roles", "Content Management"'),
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#size' => 30,
      '#maxlength' => 64,
      '#required' => TRUE,
      '#default_value' => $role_set->id(),
      '#machine_name' => [
        'exists' => '\Drupal\role_sets\Entity\RoleSet::load',
      ],
      '#disabled' => !$role_set->isNew(),
    ];

    if ($role_set->isNew()) {
      $form['set_type'] = [
        '#type' => 'radios',
        '#title' => $this->t('Role Set Type'),
        '#default_value' => 'global',
        '#options' => array(
          'global' => $this->t('Global (include all permissions from all modules)'),
          'package' => $this->t('Package (include only permissions from selected module packages)'),
          'module' => $this->t('Module (include only permissions from selected modules)'),
        ),
      ];
    }

    // Add a wrapper around the roles checkboxes.
    $form['show_roles'] = [
      '#type' => 'details',
      '#title' => $this->t('Roles'),
      '#open' => TRUE,
    ];

    $form['show_roles']['roles'] = [
      '#type' => 'checkboxes',
      '#options' => $this->getRoleOptions(),
      '#default_value' => $role_set->getRoles(),
    ];

    if ($role_set->getSetType() == 'module') {
      // Add a wrapper around the module checkboxes.
      $form['show_modules'] = [
        '#type' => 'details',
        '#title' => $this->t('Modules'),
        '#open' => TRUE,
      ];

      $form['show_modules']['modules'] = [
        '#type' => 'checkboxes',
        '#options' => $this->getModuleOptions(),
        '#default_value' => $role_set->getModules(),
      ];
    }

    if ($role_set->getSetType() == 'package') {
      // Add a wrapper around the module checkboxes.
      $form['show_packages'] = [
        '#type' => 'details',
        '#title' => $this->t('Packages'),
        '#open' => TRUE,
      ];

      $form['show_packages']['packages'] = [
        '#type' => 'checkboxes',
        '#options' => $this->getPackageOptions(),
        '#default_value' => $role_set->getPackages(),
      ];
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $role_set = $this->entity;
    $status = $role_set->save();

    $edit_link = $this->entity->link($this->t('Edit'));
    // If a new module or package type set is added, redirect back to form.
    $redirect_link = 'collection';
    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Role Set.', [
          '%label' => $role_set->label(),
        ]));
        $this->logger('role_set')->notice('Role Set %label has been added.', [
          '%label' => $role_set->label(),
          'link' => $edit_link,
        ]);

        if ($role_set->getSetType() != 'global') {
          $redirect_link = 'edit-form';
        }
        break;

      default:
        drupal_set_message($this->t('Saved the %label Role Set.', [
          '%label' => $role_set->label(),
        ]));
        $this->logger('role_set')->notice('Role Set %label has been updated.', [
          '%label' => $role_set->label(),
          'link' => $edit_link,
        ]);
    }
    $form_state->setRedirectUrl($role_set->toUrl($redirect_link));
  }

  /**
   * Gets the roles options to display in a form checkbox element.
   *
   * @return array
   *   An associative array whose keys are the values returned for each
   *   checkbox, and whose values are the labels next to each checkbox.
   */
  protected function getRoleOptions() {
    $roles = $this->roleStorage->loadMultiple();
    $options = array();
    foreach ($roles as $role) {
      $options[$role->id()] = $role->label();
    }
    return $options;
  }

  /**
   * Gets the module options to display in a form checkbox element.
   *
   * Gets only modules which have permissions (permission providers)
   * options are sorted alphabetically.
   *
   * @return array
   *   An associative array whose keys are the values returned for each
   *   checkbox, and whose values are the labels next to each checkbox.
   */
  protected function getModuleOptions() {
    $permissions = $this->permissionHandler->getPermissions();
    $permissions_by_provider = array();
    foreach ($permissions as $permission_name => $permission) {
      $permissions_by_provider[$permission['provider']][$permission_name] = $permission;
    }
    $options = array();
    foreach ($permissions_by_provider as $provider => $permissions) {
      $options[$provider] = $this->moduleHandler->getName($provider);
    }
    ksort($options);
    return $options;
  }

  /**
   * Gets the package options to display in a form checkbox element.
   *
   * Gets only packages which have modules with permissions (providers).
   * Options are sorted alphabetically.
   *
   * @return array
   *   An associative array whose keys are the values returned for each
   *   checkbox, and whose values are the labels next to each checkbox.
   */
  protected function getPackageOptions() {
    $modules = $this->moduleHandler->getModuleList();
    $modules_by_package = array();
    foreach ($modules as $module => $data) {
      $info = system_get_info('module', $module);
      if ((!empty($info)) &&
        (empty($info['hidden'])) &&
        ($this->permissionHandler->moduleProvidesPermissions($module))) {
        $modules_by_package[$info['package']][$module] = 1;
      }
    }
    $options = array();
    foreach ($modules_by_package as $package => $modules) {
      $options[$package] = $package;
    }
    ksort($options);
    return $options;
  }

}
