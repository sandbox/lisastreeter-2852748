<?php

namespace Drupal\role_sets\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\role_sets\Entity\RoleSetInterface;
use Drupal\user\Form\UserPermissionsForm;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the user permissions administration form for the role set.
 */
class UserPermissionsRoleSetForm extends UserPermissionsForm {

  /**
   * The permission handler.
   *
   * @var \Drupal\user\PermissionHandlerInterface
   */
  protected $permissionHandler;

  /**
   * The specific role set for this form.
   *
   * @var \Drupal\role_sets\Entity\RoleSetInterface
   */
  protected $roleSet;

  /**
   * {@inheritdoc}
   *
   * Note: Using custom role_sets.permissions rather than user.permissions.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('role_sets.permissions'),
      $container->get('entity.manager')->getStorage('user_role'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   *
   * Filter roles based on role set.
   */
  protected function getRoles() {
    $roles = array();
    foreach ($this->roleSet->getRoles() as $role_id) {
      $roles[$role_id] = \Drupal::entityManager()->getStorage('user_role')->load($role_id);
    }
    return $roles;
  }

  /**
   * {@inheritdoc}
   *
   * @param RoleSetInterface $role_set
   *   The user role set used for this form.
   */
  public function buildForm(array $form, FormStateInterface $form_state, RoleSetInterface $role_set = NULL) {
    $this->roleSet = $role_set;
    $this->permissionHandler->setRoleSet($role_set);
    $form = parent::buildForm($form, $form_state);
    return $form;
  }

}
