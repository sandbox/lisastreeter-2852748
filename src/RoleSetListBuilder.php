<?php

namespace Drupal\role_sets;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\role_sets\Entity\RoleSetInterface;

/**
 * Provides a listing of Role set entities.
 */
class RoleSetListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Role set');
    $header['roles'] = $this->t('Roles');
    $header['set_type'] = $this->t('Set type');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = $entity->label();
    $row['rows'] = $this->getRoleList($entity);
    $row['set_type'] = ucfirst($entity->getSetType());
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity) {
    $operations = parent::getDefaultOperations($entity);

    if ($entity->hasLinkTemplate('edit-permissions-form')) {
      $operations['permissions'] = array(
        'title' => t('Edit permissions'),
        'weight' => 20,
        'url' => $entity->urlInfo('edit-permissions-form'),
      );
    }
    return $operations;
  }

  /**
   * Returns a list as a comma-separated string of role labels.
   *
   * @param \Drupal\role_sets\Entity\RoleSetInterface $entity
   *   The role set from which to get the roles.
   *
   * @return string
   *   The list of role labels.
   */
  protected function getRoleList(RoleSetInterface $entity) {
    $storage = \Drupal::entityManager()->getStorage('user_role');
    $roles = $entity->getRoles();
    $role_list = array();
    foreach ($roles as $role) {
      $role_entity = $storage->load($role);
      if ($role_entity) {
        $role_list[] = $role_entity->label();
      }
    }
    return implode(', ', $role_list);
  }

}
