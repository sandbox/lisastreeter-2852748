<?php

namespace Drupal\role_sets;

use Drupal\user\PermissionHandler;
use Drupal\role_sets\Entity\RoleSetInterface;

/**
 * Handler for role_sets.permissions service.
 *
 * {@inheritdoc}
 */
class RoleSetPermissionHandler extends PermissionHandler {

  /**
   * The role set.
   *
   * @var \Drupal\role_sets\Entity\RoleSetInterface
   */
  protected $roleSet;

  /**
   * Sets the role set for the permission handler.
   *
   * @param RoleSetInterface $role_set
   *   The role set.
   */
  public function setRoleSet(RoleSetInterface $role_set) {
    $this->roleSet = $role_set;
  }

  /**
   * {@inheritdoc}
   *
   * Filter permissions based on modules or packages specified by the role set.
   */
  public function getPermissions() {

    // Filter module type permissions by provider name.
    if ((isset($this->roleSet)) && ($this->roleSet->getSetType() == 'module')) {
      $role_set_modules = $this->roleSet->getModules();
      $all_permissions = $this->getModulePermissions($role_set_modules);

    }
    elseif ((isset($this->roleSet)) &&
      ($this->roleSet->getSetType() == 'package')) {
      $role_set_packages = $this->roleSet->getPackages();

      // Filter modules using package list.
      $modules = $this->moduleHandler->getModuleList();
      $role_set_modules = array();

      // Include module if not hidden and matches package in role set.
      foreach ($modules as $module => $data) {
        $info = system_get_info('module', $module);
        if ((!empty($info)) && (empty($info['hidden'])) &&
          (array_key_exists($info['package'], $role_set_packages))) {
          $role_set_modules[$module] = $data;
        }
      }
      $all_permissions = $this->getModulePermissions($role_set_modules);

    }
    else {
      // Default is global role set, all permissions.
      $all_permissions = $this->buildPermissionsYaml();
    }

    return $this->sortPermissions($all_permissions);
  }

  /**
   * Returns array of all permissions for the specified modules.
   *
   * @param array $role_set_modules
   *   The modules for which to include permissions.
   *
   * @return array[]
   *   Each return permission is an array with the following keys:
   *   - title: The title of the permission.
   *   - description: The description of the permission, defaults to NULL.
   *   - provider: The provider of the permission.
   */
  protected function getModulePermissions(array $role_set_modules = array()) {
    $all_permissions = $this->buildPermissionsYaml();
    $module_permissions = array();

    foreach ($all_permissions as $permission_name => $permission) {
      if (array_key_exists($permission['provider'], $role_set_modules)) {
        $module_permissions[$permission_name] = $permission;
      }
    }
    return $module_permissions;
  }

}
