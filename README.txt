CONTENTS OF THIS FILE
---------------------
   
 * Introduction 
 * Requirements 
 * Recommended modules 
 * Installation 
 * Configuration
 * Troubleshooting 
 * FAQ

INTRODUCTION
------------

If you have a large number of User Roles, the Role Sets module can help you
organize them and manage their permissions more easily. Using the User Roles
module makes sense when you are defining roles as capabilities, not job titles.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/sandbox/lisastreeter/2852748

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/2852748

REQUIREMENTS
------------

No special requirements

RECOMMENDED MODULES
-------------------

 * User Personas (https://www.drupal.org/project/personas):
   With personas installed, configurable roles per user are replaced by a
   selection of personas. Personas describe a group of your users and personas
   can have many roles.

INSTALLATION
------------
 
 * Install as you would normally install a contributed Drupal module. See:
   https://www.drupal.org/documentation/install/modules-themes/modules-8
   for further information.

CONFIGURATION
-------------
 
 * Configure user permissions in Administration » People » Permissions:

   - Use the administration pages and help (System module)

     The top-level administration categories require this permission to be
     accessible. The administration menu will be empty unless this permission
     is granted.

   - View role sets

     Users in roles with the "View role sets" permission will see the Role sets
     tab at the top of the People administration pages.

   - Administer role sets

     Users in roles with the "Administer role sets" permission will be able to
     create new role sets and modify existing role sets.

   - Set permissions for role sets

     Users in roles with both the "Administer role sets" permission and the
     "Administer permissions" (User module) permission will be able to set
     permissions for roles within role sets.

 * Customize the role sets in Administration » People » Role sets.

TROUBLESHOOTING
---------------

The module currently has no known issues.

FAQ
---

No FAQs yet exist.
